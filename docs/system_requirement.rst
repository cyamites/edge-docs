.. _sys_requirement:

System requirements
###################
NOTE: The web-based online version of EDGE, found on https://bioedge.lanl.gov/edge_ui/ is run on our own internal servers and is our recommended mode of usage for EDGE. It does not require any particular hardware or software other than a web browser. This segment and the installation segment only apply if you want to run EDGE through Python or Apache 2, or through the CLI. 

The current version of the EDGE pipeline has been extensively tested on a Linux Server with Ubuntu 14.04 and Centos 6.5 and 7.0 operating system and will work on 64bit Linux environments. Perl v5.8 or above is required. Python 2.7 is required. Due to the involvement of several memory/time consuming steps, it requires at least 16Gb memory and at least 8 computing CPUs. A higher computer spec is recommended: 128Gb memory and 16 computing CPUs.

Please ensure that your system has the essential software building packages installed properly before running the installing script.

The following are required installed by system administrator.

.. note:: If your system OS is neither Ubuntu 14.04 or Centos 6.5 or 7.0, it may have differnt packages/libraries name and the newer complier (gcc5) on newer OS (ex: Ubuntu 16.04) may fail on compling some of thirdparty bioinformatics tools. We would suggest to use EDGE `VMware image <installation.html#edge-vmware-ovf-image>`_ or `Docker container <installation.html#edge-docker-image>`_.  

Ubuntu 14.04 
============

.. image:: https://design.ubuntu.com/wp-content/uploads/ubuntu-logo14.png
    :width: 200px

1. Install build essential libraries and dependancies::
 
    sudo apt-get -y update
    sudo apt-get install -y build-essential libreadline-gplv2-dev libx11-dev \
      libxt-dev libgsl0-dev libfreetype6-dev libncurses5-dev gfortran \
      inkscape libwww-perl libxml-libxml-perl libperlio-gzip-perl  \
      zlib1g-dev zip unzip libjson-perl libpng12-dev cpanminus default-jre \
      firefox wget curl csh liblapack-dev libblas-dev libatlas-dev \
      libcairo2-dev libssh2-1-dev libssl-dev libcurl4-openssl-dev bzip2 \
      bioperl rsync libbz2-dev liblzma-dev time libterm-readkey-perl
    
2. Install Apache2 for EDGE UI::
    
    sudo apt-get install apache2
    sudo a2enmod cgid proxy proxy_http headers

3. Install packages for user management system::

    sudo apt-get install sendmail mysql-client mysql-server phpMyAdmin tomcat7

CentOS 6.7
==========

.. image:: https://scottlinux.com/wp-content/uploads/2011/07/centos6.png
    :width: 200px
    
1. Install dependancies using yum::

    # add epel reporsitory 
    sudo yum -y install epel-release
    su -c 'yum localinstall -y --nogpgcheck http://download1.rpmfusion.org/free/el/updates/6/i386/rpmfusion-free-release-6-1.noarch.rpm http://download1.rpmfusion.org/nonfree/el/updates/6/i386/rpmfusion-nonfree-release-6-1.noarch.rpm'
    sudo yum -y update
    
    sudo yum -y install\
     csh gcc gcc-c++ make curl binutils gd gsl-devel \
     libX11-devel readline-devel libXt-devel ncurses-devel inkscape \
     freetype freetype-devel zlib zlib-devel git \
     blas-devel atlas-devel lapack-devel libpng libpng-devel \
     cairo-devel openssl-devel libssh2-devel libcurl-devel \
     expat expat-devel graphviz java-1.7.0-openjdk \
     perl-Archive-Zip perl-Archive-Tar perl-CGI perl-CGI-Session \
     perl-DBI perl-GD perl-JSON perl-Module-Build perl-CPAN-Meta-YAML \
     perl-XML-LibXML perl-XML-Parser perl-XML-SAX perl-XML-SAX-Writer \
     perl-XML-Simple perl-XML-Twig perl-XML-Writer perl-YAML \
     perl-Test-Most perl-PerlIO-gzip perl-SOAP-Lite perl-GraphViz \
     wget bzip2 rsync bzip2-devel xz-devel time

2. Install perl cpanm::

    curl -L http://cpanmin.us | perl - App::cpanminus
    
3. Install perl modules by cpanm::

    cpanm Graph Time::Piece Data::Dumper IO::Compress::Gzip Data::Stag IO::String
    cpanm Algorithm::Munkres Array::Compare Clone Convert::Binary::C XML::Parser::PerlSAX
    cpanm HTML::Template HTML::TableExtract List::MoreUtils PostScript::TextBlock
    cpanm SVG SVG::Graph Set::Scalar Sort::Naturally Spreadsheet::ParseExcel Term::ReadKey
    cpanm -f Bio::Perl

4. Install package for httpd for EDGE UI::
    
    sudo yum -y install httpd

5. Install packages for user management system::

    sudo yum -y install sendmail mysql mysql-server phpmyadmin tomcat

CentOS 7
========

.. image:: https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Centos_full.svg/200px-Centos_full.svg.png
    :width: 200px

1. Install libraries and dependencies by yum::

    # add epel reporsitory 
    sudo yum -y install epel-release
    
    sudo yum install -y libX11-devel readline-devel libXt-devel ncurses-devel inkscape \ 
        expat expat-devel freetype freetype-devel zlib zlib-devel perl-App-cpanminus \
        perl-Test-Most blas-devel atlas-devel lapack-devel libpng12 libpng12-devel \
        perl-XML-Simple perl-JSON csh gcc gcc-c++ make binutils gd gsl-devel git graphviz \
        java-1.7.0-openjdk perl-Archive-Zip perl-CGI curl perl-CGI-Session \
        perl-CPAN-Meta-YAML perl-DBI perl-Data-Dumper perl-GD perl-IO-Compress \
        perl-Module-Build perl-XML-LibXML perl-XML-Parser perl-XML-SAX perl-XML-SAX-Writer \
        perl-XML-Twig perl-XML-Writer perl-YAML perl-PerlIO-gzip libstdc++-static \
        cairo-devel openssl-devel openssl-static libssh2-devel libcurl-devel \
        wget rsync bzip2 bzip2-devel xz-devel time
        
2. Update perl tools::
    
    sudo cpanm App::cpanoutdated
    sudo su -
    cpan-outdated -p | cpanm
    exit

3. Install perl modules by cpanm::
    
    sudo cpanm -f Bio::Perl
    sudo cpanm Graph Time::Piece Hash::Merge PerlIO::gzip Heap::Simple::XS File::Next
    sudo cpanm Algorithm::Munkres Archive::Tar Array::Compare Clone Convert::Binary::C
    sudo cpanm HTML::Template HTML::TableExtract List::MoreUtils PostScript::TextBlock
    sudo cpanm SOAP::Lite SVG SVG::Graph Set::Scalar Sort::Naturally Spreadsheet::ParseExcel
    sudo cpanm CGI::Simple GraphViz XML::Parser::PerlSAX XML::Simple Term::ReadKey

4. Install package for httpd for EDGE UI::
    
    sudo yum -y install httpd
    sudo systemctl enable httpd && sudo systemctl start httpd
  
5. Install packages for user management system::
    
    sudo yum -y install sendmail mariadb-server mariadb php phpMyAdmin tomcat
    sudo systemctl enable tomcat && sudo systemctl start tomcat

6. Configure firewall for ssh, http, https, and smtp::
    
    sudo firewall-cmd --permanent --add-service=ssh
    sudo firewall-cmd --permanent --add-service=http
    sudo firewall-cmd --permanent --add-service=https
    sudo firewall-cmd --permanent --add-service=smtp
    sudo firewall-cmd --reload

7. Disable SELinux::

    As root edit /etc/selinux/config and set SELINUX=disabled
	
	Restart the server to make the change


