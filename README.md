EDGE Documentation
==================

Documentation for the EDGE bioinformatics project, a DTRA-funded NGS analysis effort.

* [Read The Docs](http://edge.readthedocs.org/)

* [PDF](https://readthedocs.org/projects/edge/downloads/pdf/latest/)

* [ebook](https://readthedocs.org/projects/edge/downloads/epub/latest/)

![ebook](https://bytebucket.org/nmrcjoe/edge-docs/raw/a224a36166901772efc558343007c6b25a131aa4/ebook.png)
